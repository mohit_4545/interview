//
//  ViewController.swift
//  MiniAssignment
//
//  Created by Mohit Gupta on 24/09/1942 Saka.
//

import UIKit

class ViewController: UIViewController{
    
    @IBOutlet var inputTxt: UITextField?{
        didSet{
            inputTxt?.placeholder = "Input String...."
        }
    }
    @IBOutlet var nTimesTxt: UITextField?{
        didSet{
            nTimesTxt?.placeholder = "N..."
        }
    }
    @IBOutlet var outputTxt: UITextField?{
        didSet{
            outputTxt?.placeholder = "Output String..."
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        print(encode(str: "abc", k: 2))
        
    }
    
    @IBAction func callAlgo(){
        if self.inputTxt?.text != nil,self.inputTxt?.text != ""{
            //            print(self.encode(str: self.inputTxt?.text ?? "", k: nTimesTxt?.text?.utf8 ?? 0))
            print(encode(str: "abc", k: 28))
            outputTxt?.text = (encode(str: "abc", k: 28))
        }else{
            
        }
    }
    
}

extension ViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == nTimesTxt{
            
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
            
        }else if textField == inputTxt{
            
            let characterSet = CharacterSet.letters 
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                return false
            }
            return true
        }else{
            return false
        }
    }
}


extension ViewController{
    func encode(str:String, k:UInt8) -> String{
        // changed string
        var newStr = ""
        
        var kChange: UInt8
        
        // iterate for every characters
        
        for char in str {
            print("Found character: \(char)")
            kChange = k
            // ASCII value
            let AVal = char.asciiValue!
            
            //store the duplicate
            let kChangeDup = k
            
            if (AVal + kChange) > 122{
                
                kChange = kChange - (122 - AVal)
                kChange = kChange % 26
                newStr = newStr +  String(UnicodeScalar(UInt8(96 + kChange)))
            }else{
                newStr = newStr + String(UnicodeScalar(UInt8(AVal + kChange)))
                
                kChange = kChangeDup
            }
        }
        return newStr
    }
}


extension StringProtocol {
    var asciiValues: [UInt8] { compactMap(\.asciiValue) }
}
