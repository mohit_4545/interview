//
//  ProductViewController.swift
//  MiniAssignment
//
//  Created by Mohit Gupta on 18/12/20.
//

import UIKit
import Alamofire
import AlamofireImage

class ProductViewController: UIViewController {
    
    
    @IBOutlet weak var sliderCollectionView: UICollectionView!
    @IBOutlet weak var pageView: UIPageControl!
    @IBOutlet weak var uiTableView: UITableView!
    
    var imgArr = [ UIImage(named:"Alexandra Daddario"),
                   UIImage(named:"Angelina Jolie") ,
                   UIImage(named:"Anne Hathaway")
    ]
    
    var timer = Timer()
    var counter = 0
    var presentor:ViewToPresenterProtocol?
    var productArrayList:Array<ProductModel> = Array()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Home-Module"
        presentor?.startFetchingProduct()
        //        showProgressIndicator(view: self.view)
        
        uiTableView.delegate = self
        uiTableView.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.sliderInit()
    }
    
    func sliderInit(){
        pageView.numberOfPages = imgArr.count
        pageView.currentPage = 0
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
    }
    
}

extension ProductViewController:PresenterToViewProtocol{
    
    func showProduct(ProductArray: Array<ProductModel>) {
        
        self.productArrayList = ProductArray
        self.uiTableView.reloadData()
        //        hideProgressIndicator(view: self.view)
        
    }
    
    func showError() {
        
        //        hideProgressIndicator(view: self.view)
        let alert = UIAlertController(title: "Alert", message: "Problem Fetching Product", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
}

extension ProductViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productArrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductCell
        //        cell.id.text = productArrayList[indexPath.row].id
        cell.title.text = productArrayList[indexPath.row].title
        cell.brief.text = productArrayList[indexPath.row].brief
        cell.imagesource.text = productArrayList[indexPath.row].imagesource
        
        cell.btn.addTarget(self, action: #selector(click), for: .touchUpInside)
        
        Alamofire.request(self.productArrayList[indexPath.row].imagesource!).responseData { (response) in
            if response.error == nil {
                print(response.result)
                if let data = response.data {
                    cell.bgimage.roundCorners([.topLeft, .topRight], radius: 50)
                    
                    cell.bgimage.image = UIImage(data: data)
                }
            }
        }
        
        //        cell.price.text = productArrayList[indexPath.row].price
        //        cell.quantity.text = productArrayList[indexPath.row].quantity
        //
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
    
}


class ProductCell:UITableViewCell{
    
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var brief: UILabel!
    @IBOutlet weak var imagesource: UILabel!
    @IBOutlet weak var bgimage: UIImageView!
    @IBOutlet weak var btn: UIButton!{
        didSet{
            btn.setTitle("Add", for: .normal)
            btn.titleLabel?.textColor = .white
            btn.backgroundColor = UIColor.green
            
            //            btn.backgroundColor = .clear
            btn.layer.cornerRadius = 20
            //            btn.layer.borderWidth = 1
            //            btn.layer.borderColor = UIColor.black.cgColor
        }
    }
    //    @IBOutlet weak var price: UILabel!
    //    @IBOutlet weak var quantity: UILabel!
    
    
    
    
}


extension ProductViewController{
    
    @objc @IBAction func click(){
        presentor?.showListController(navigationController: navigationController!)
    }
    
    @objc func changeImage() {
        
        if counter < imgArr.count {
            let index = IndexPath.init(item: counter, section: 0)
            self.sliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageView.currentPage = counter
            counter += 1
        } else {
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            self.sliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            pageView.currentPage = counter
            counter = 1
        }
        
    }
}


extension ProductViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        if let vc = cell.viewWithTag(111) as? UIImageView {
            vc.image = imgArr[indexPath.row]
        }
        return cell
    }
}


extension ProductViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = sliderCollectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
