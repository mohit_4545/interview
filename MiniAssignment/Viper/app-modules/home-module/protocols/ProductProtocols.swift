//
//  ProductProtocols.swift
//  MiniAssignment
//
//  Created by Mohit Gupta on 18/12/20.
//
import Foundation
import UIKit


protocol ViewToPresenterProtocol: class {
    var view: PresenterToViewProtocol? {get set}
    var interactor: PresenterToInteractorProtocol? {get set}
    var router: PresenterToRouterProtocol? {get set}
    func startFetchingProduct()
    func showListController(navigationController:UINavigationController)
}

protocol PresenterToViewProtocol: class{
    func showProduct(ProductArray:Array<ProductModel>)
    func showError()
}

protocol PresenterToInteractorProtocol: class {
    var presenter:InteractorToPresenterProtocol? {get set}
    func fetchProduct()
}

protocol InteractorToPresenterProtocol: class {
    func productFetchedSuccess(productModelArray:Array<ProductModel>)
    func productFetchFailed()
}

protocol PresenterToRouterProtocol: class {
    static func createModule()-> ProductViewController
    func pushToScreen(navigationConroller:UINavigationController)
}
