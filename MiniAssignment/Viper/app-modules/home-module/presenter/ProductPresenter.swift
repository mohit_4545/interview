//
//  ProductPresenter.swift
//  MiniAssignment
//
//  Created by Mohit Gupta on 18/12/20.
//
import Foundation
import UIKit

class ProductPresenter: ViewToPresenterProtocol {
    
    var view: PresenterToViewProtocol?
    
    var interactor: PresenterToInteractorProtocol?
    
    var router: PresenterToRouterProtocol?
    
    func startFetchingProduct() {
        interactor?.fetchProduct()
    }
    
    func showListController(navigationController: UINavigationController) {
        router?.pushToScreen(navigationConroller:navigationController)
    }

}


extension ProductPresenter: InteractorToPresenterProtocol{
     
    
    
    func productFetchedSuccess(productModelArray: Array<ProductModel>) {
        view?.showProduct(ProductArray: productModelArray)
    }
    
    func productFetchFailed() {
        view?.showError()
    }
    
    
}
