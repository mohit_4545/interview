//
//  ProductRouter.swift
//  MiniAssignment
//
//  Created by Mohit Gupta on 18/12/20.
//
import Foundation
import UIKit

class ProductRouter: PresenterToRouterProtocol{
    
    static func createModule() -> ProductViewController {
        
        let view = mainstoryboard.instantiateViewController(withIdentifier: "ProductViewControllerID") as! ProductViewController
        
        let presenter: ViewToPresenterProtocol & InteractorToPresenterProtocol = ProductPresenter()
        
        
        let interactor: PresenterToInteractorProtocol = ProductInteractor()
        let router:PresenterToRouterProtocol = ProductRouter()
        
        view.presentor = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
        
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: nil)
    }
    
    func pushToScreen(navigationConroller navigationController:UINavigationController) {
        
        let listModue = ListRouter.createListModule()
        navigationController.pushViewController(listModue,animated: true)
        
    }
    
}
