//
//  CartProtocol.swift
//  VIPER-demo
//
//  Created by Bipin on 7/2/18.
//  Copyright © 2018 Tootle. All rights reserved.
//

import Foundation

protocol ViewToPresenterCartProtocol:class{
    
    var view: PresenterToViewCartProtocol? {get set}
    var interactor: PresenterToInteractorCartProtocol? {get set}
    var router: PresenterToRouterCartProtocol? {get set}
    func startFetchingCart()
    

}

protocol PresenterToViewCartProtocol:class {
    
    func onCartResponseSuccess(CartModelArrayCart:Array<CartModel>)
    func onCartResponseFailed(error:String)
    
}

protocol PresenterToRouterCartProtocol:class {
    
    static func createCartModule()->CartViewController

}

protocol PresenterToInteractorCartProtocol:class {
    
    var presenter:InteractorToPresenterCartProtocol? {get set}
    func fetchCart()
    
}

protocol InteractorToPresenterCartProtocol:class {
    
    func CartFetchSuccess(CartModelArray:Array<CartModel>)
    func CartFetchFailed()
    
}
