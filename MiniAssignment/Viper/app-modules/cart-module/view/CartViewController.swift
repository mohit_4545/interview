//
//  CartViewController.swift
//  MiniAssignment
//
//  Created by Mohit Gupta on 20/12/20.
//

import UIKit
import Alamofire
import AlamofireImage

class CartViewController: UIViewController {
    @IBOutlet weak var uiTableView: UITableView!
    
    
    var CartPresenter:ViewToPresenterCartProtocol?
    var CartArrayCart:Array<CartModel> = Array()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Cart-Module"
        CartPresenter?.startFetchingCart()
        //        showProgressIndicator(view: self.view)
        
        uiTableView.delegate = self
        uiTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    

    

}

extension CartViewController:PresenterToViewCartProtocol{
    func onCartResponseSuccess(CartModelArrayCart: Array<CartModel>) {
        self.CartArrayCart = CartModelArrayCart
        self.uiTableView.reloadData()
//        hideProgressIndicator(view: self.view)
    }
    
    func onCartResponseFailed(error: String) {
        //        hideProgressIndicator(view: self.view)
                let alert = UIAlertController(title: "Alert", message: "Problem Fetching Cart", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
    }
    
 
    
    
}
extension CartViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CartArrayCart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductCell
//        cell.id.text = CartArrayCart[indexPath.row].id
        cell.title.text = CartArrayCart[indexPath.row].title
        cell.brief.text = CartArrayCart[indexPath.row].brief
//        cell.imagesource.text = CartArrayCart[indexPath.row].imagesource
        
        Alamofire.request(self.CartArrayCart[indexPath.row].imagesource!).responseData { (response) in
            if response.error == nil {
                print(response.result)
                                if let data = response.data {
                                    

                    cell.bgimage.image = UIImage(data: data)
                }
            }
        }
        
//        cell.price.text = CartArrayCart[indexPath.row].price
//        cell.quantity.text = CartArrayCart[indexPath.row].quantity
//
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
         
        
    }
    
    
}
