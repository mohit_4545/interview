//
//  CartPresenter.swift
//  VIPER-demo
//
//  Created by Bipin on 7/2/18.
//  Copyright © 2018 Tootle. All rights reserved.
//

import Foundation

class CartPresenter:ViewToPresenterCartProtocol{
    
    var view: PresenterToViewCartProtocol?
    
    var interactor: PresenterToInteractorCartProtocol?
    
    var router: PresenterToRouterCartProtocol?
    
    func startFetchingCart() {
        interactor?.fetchCart()
    }
    
}

extension CartPresenter:InteractorToPresenterCartProtocol{
    
    func CartFetchSuccess(CartModelArray: Array<CartModel>) {
        view?.onCartResponseSuccess(CartModelArrayCart: CartModelArray)
    }
    
    func CartFetchFailed() {
        view?.onCartResponseFailed(error: "Some Error message from api response")
    }
    
    
}
