//
//  CartRouter.swift
//  VIPER-demo
//
//  Created by Bipin on 7/2/18.
//  Copyright © 2018 Tootle. All rights reserved.
//

import Foundation
import UIKit

class CartRouter:PresenterToRouterCartProtocol{
    
    static func createCartModule() -> CartViewController {
        
        let view = CartRouter.mainstoryboard.instantiateViewController(withIdentifier: "CartViewControllerID") as! CartViewController
        
        let presenter: ViewToPresenterCartProtocol & InteractorToPresenterCartProtocol = CartPresenter()
        let interactor: PresenterToInteractorCartProtocol = CartInteractor()
        let router:PresenterToRouterCartProtocol = CartRouter()
        
        view.CartPresenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
        
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: nil)
    }
    
}
