//
//  ListRouter.swift
//  VIPER-demo
//
//  Created by Bipin on 7/2/18.
//  Copyright © 2018 Tootle. All rights reserved.
//

import Foundation
import UIKit

class ListRouter:PresenterToRouterListProtocol{
    
    static func createListModule() -> ListViewController {
        
        let view = ListRouter.mainstoryboard.instantiateViewController(withIdentifier: "ListViewControllerID") as! ListViewController
        
        let presenter: ViewToPresenterListProtocol & InteractorToPresenterListProtocol = ListPresenter()
        let interactor: PresenterToInteractorListProtocol = ListInteractor()
        let router:PresenterToRouterListProtocol = ListRouter()
        
        view.ListPresenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        return view
        
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: nil)
    }
    
    
    func pushToScreen1(navigationConroller navigationController:UINavigationController) {
            
            let cartModue = CartRouter.createCartModule()
            navigationController.pushViewController(cartModue,animated: true)
            
        }
    
}
