//
//  ListProtocol.swift
//  VIPER-demo
//
//  Created by Bipin on 7/2/18.
//  Copyright © 2018 Tootle. All rights reserved.
//

import Foundation
import UIKit

protocol ViewToPresenterListProtocol:class{
    
    var view: PresenterToViewListProtocol? {get set}
    var interactor: PresenterToInteractorListProtocol? {get set}
    var router: PresenterToRouterListProtocol? {get set}
    func startFetchingList()
    
    func showCartController1(navigationController:UINavigationController)

}

protocol PresenterToViewListProtocol:class {
    
    func onListResponseSuccess(ListModelArrayList:Array<ListModel>)
    func onListResponseFailed(error:String)
    
}

protocol PresenterToRouterListProtocol:class {
    
    static func createListModule()->ListViewController
    
    func pushToScreen1(navigationConroller:UINavigationController)

}

protocol PresenterToInteractorListProtocol:class {
    
    var presenter:InteractorToPresenterListProtocol? {get set}
    func fetchList()
    
}

protocol InteractorToPresenterListProtocol:class {
    
    func ListFetchSuccess(ListModelArray:Array<ListModel>)
    func ListFetchFailed()
    
}


