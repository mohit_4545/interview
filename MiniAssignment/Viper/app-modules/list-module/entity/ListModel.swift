//
//  ListModel.swift
//  MiniAssignment
//
//  Created by Mohit Gupta on 18/12/20.
//

import UIKit
import ObjectMapper


private let ID = "id"
private let TITLE = "title"
private let BRIEF = "brief"
private let IMAGESOURCE = "image_url"
//private let PRICE = "price"
//private let QUANTITY = "quantity"

class ListModel: Mappable {

    
    internal var id:String?
    internal var title:String?
    internal var brief:String?
    internal var imagesource:String?
//    internal var price:String?
//    internal var quantity:String?
    
    required init?(map:Map) {
        mapping(map: map)
    }
    
    func mapping(map:Map){
        id <- map[ID]
        title <- map[TITLE]
        brief <- map[BRIEF]
        imagesource <- map[IMAGESOURCE]
//        price <- map[PRICE]
//        quantity <- map[QUANTITY]
    }
    
    
}
