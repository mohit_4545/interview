//
//  ListPresenter.swift
//  VIPER-demo
//
//  Created by Bipin on 7/2/18.
//  Copyright © 2018 Tootle. All rights reserved.
//

import Foundation
import UIKit

class ListPresenter:ViewToPresenterListProtocol{
    
    var view: PresenterToViewListProtocol?
    
    var interactor: PresenterToInteractorListProtocol?
    
    var router: PresenterToRouterListProtocol?
    
    func startFetchingList() {
        interactor?.fetchList()
    }
    
    
    func showCartController1(navigationController: UINavigationController) {
        router?.pushToScreen1(navigationConroller:navigationController)
    }
    
    
}

extension ListPresenter:InteractorToPresenterListProtocol{
    
    func ListFetchSuccess(ListModelArray: Array<ListModel>) {
        view?.onListResponseSuccess(ListModelArrayList: ListModelArray)
    }
    
    func ListFetchFailed() {
        view?.onListResponseFailed(error: "Some Error message from api response")
    }
    
    
}
