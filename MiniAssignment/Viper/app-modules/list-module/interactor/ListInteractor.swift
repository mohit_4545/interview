//
//  ProductInteractor.swift
//  MiniAssignment
//
//  Created by Mohit Gupta on 18/12/20.
//

import Foundation
import Alamofire
import ObjectMapper

class ListInteractor: PresenterToInteractorListProtocol {
    var presenter: InteractorToPresenterListProtocol?
    
    func fetchList() {
         
        if let url = Bundle.main.url(forResource: "response", withExtension: "json") {
                do {
                    let data = try Data(contentsOf: url)
                    if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as AnyObject?{
                        let arrayResponse = json["movie_list"] as! NSArray
                        let arrayObject = Mapper<ListModel>().mapArray(JSONArray: arrayResponse as! [[String : Any]]);
                        print(arrayObject)
                        
                        self.presenter?.ListFetchSuccess(ListModelArray: arrayObject)
                     
                         
                    }
                     
                     
                } catch {
                     
                }
            }
        
        
//        Alamofire.request(API_MOVIE_LIST).responseJSON { response in
//
//            if(response.response?.statusCode == 200){
//                if let json = response.result as AnyObject? {
//                    let arrayResponse = json["movie_list"] as! NSArray
//                    let arrayObject = Mapper<ProductModel>().mapArray(JSONArray: arrayResponse as! [[String : Any]]);
//                    self.presenter?.productFetchedSuccess(productModelArray: arrayObject)
//                }
//            }else {
//                self.presenter?.productFetchFailed()
//            }
//        }
    }
    

}
