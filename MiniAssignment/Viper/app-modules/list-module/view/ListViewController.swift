//
//  ListViewController.swift
//  MiniAssignment
//
//  Created by Mohit Gupta on 20/12/20.
//

import UIKit
import Alamofire
import AlamofireImage

class ListViewController: UIViewController {
    @IBOutlet weak var uiTableView: UITableView!
    
    
    var ListPresenter:ViewToPresenterListProtocol?
    var ListArrayList:Array<ListModel> = Array()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "List-Module"
        ListPresenter?.startFetchingList()
        //        showProgressIndicator(view: self.view)
        
        uiTableView.delegate = self
        uiTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    

    

}

extension ListViewController:PresenterToViewListProtocol{
    func onListResponseSuccess(ListModelArrayList: Array<ListModel>) {
        self.ListArrayList = ListModelArrayList
        self.uiTableView.reloadData()
//        hideProgressIndicator(view: self.view)
    }
    
    func onListResponseFailed(error: String) {
        //        hideProgressIndicator(view: self.view)
                let alert = UIAlertController(title: "Alert", message: "Problem Fetching List", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
    }
    
 
    
    
}
extension ListViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 500.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ListArrayList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ProductCell
//        cell.id.text = ListArrayList[indexPath.row].id
        cell.title.text = ListArrayList[indexPath.row].title
        cell.brief.text = ListArrayList[indexPath.row].brief
        cell.imagesource.text = ListArrayList[indexPath.row].imagesource
        
        cell.btn.addTarget(self, action: #selector(click), for: .touchUpInside)
        
        Alamofire.request(self.ListArrayList[indexPath.row].imagesource!).responseData { (response) in
            if response.error == nil {
                print(response.result)
                                if let data = response.data {
                                    cell.bgimage.roundCorners([.topLeft, .topRight], radius: 50)

                    cell.bgimage.image = UIImage(data: data)
                }
            }
        }
        
//        cell.price.text = ListArrayList[indexPath.row].price
//        cell.quantity.text = ListArrayList[indexPath.row].quantity
//
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
         
        
    }
    
    
}

extension ListViewController{
    @objc @IBAction func click(){
        ListPresenter?.showCartController1(navigationController: navigationController!)
    }
    
    
    
}
